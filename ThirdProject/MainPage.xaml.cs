﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace ThirdProject
{

    public struct User
    {
        public string username;
        public string password;

        public User(string username, string password)
        {
            this.username = username;
            this.password = password;
        }
    }

    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        User[] users;
        int tries = 0;
        public MainPage()
        {
            this.InitializeComponent();
            users = new User[3];
            users[0] = new User("*CareFully*", "Care123");
            users[1] = new User("Admin", "Admin");
            users[2] = new User("Lul", "12345");
        }
        
        private void inputPassword_KeyDown(object sender, KeyRoutedEventArgs e)
        {
            // Log in if user presses enter while password textBox is active
            if (e.Key == Windows.System.VirtualKey.Enter)
            {
                doLogin();
            }
        }

        private void loginButton_Click(object sender, RoutedEventArgs e)
        {
            doLogin();
        }

        private void doLogin()
        {
            if (tries >= 3)
            {
                loginErrorText.Visibility = Visibility.Visible;
                loginErrorText.Text = "Too many tries, you're blocked!";
                return;
            }
            tries++;
            string username, password;
            username = inputUsername.Text;
            password = inputPassword.Password;

            User currentUser;
            int status = 0; // 0 - wrong username, 1 - wrong password, 2 - success

            foreach (User user in users)
            {
                if (!user.username.Equals(username))
                {
                    status = 0;
                }
                else if (!user.password.Equals(password))
                {
                    status = 1;
                    // We can break here since we found the user and there SHOULD NOT be multiple users with same username.
                    break;
                }
                else
                {
                    currentUser = user;
                    status = 2;
                    break;
                }
            }

            if (status == 2)
            {
                this.Frame.Navigate(typeof(SuccessPage));
            }
            else
            {
                loginErrorText.Visibility = Visibility.Visible;
                if (status == 0)
                {
                    loginErrorText.Text = "This user does not exist!";
                }
                else
                {
                    loginErrorText.Text = "Invalid password!";
                }
            }
        }
    }
}
